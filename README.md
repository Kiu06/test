# CITRUS FRONT END TASK

Project built width vanilla JavaScript, Bootstrap, Webpack.
Please follow the [AIRBNB](https://github.com/airbnb/javascript#airbnb-javascript-style-guide-) and [BEM](http://getbem.com/) naming conventions.

## Development

To run project:

```
$ npm start
```

Project will run on: http://localhost:8080.

To make a production build:

```
$ npm run build:prod
```
