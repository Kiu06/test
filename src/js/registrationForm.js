const phoneGroup = document.querySelector("#phoneGroup");
const emailGroup = document.querySelector("#emailGroup");
const phoneInput = document.querySelector("#phone");
const emailInput = document.querySelector("#email");
const output = document.querySelector("#output");
const phoneError = document.querySelector("#phone-error");

export class RegistrationForm {
  static selectMobile() {
    phoneGroup.classList.add("active");
    emailGroup.classList.remove("active");
    phoneInput.required = true;
    phoneInput.setAttribute("data-validate", false);
    emailInput.required = false;
    emailInput.value = "";
    emailInput.setAttribute("data-validate", true);
    output.textContent = "";
    phoneError.textContent = "";
  }

  static selectEmail() {
    phoneGroup.classList.remove("active");
    emailGroup.classList.add("active");
    phoneInput.required = false;
    phoneInput.value = "";
    phoneInput.setAttribute("data-validate", true);
    emailInput.required = true;
    emailInput.setAttribute("data-validate", false);
    phoneError.textContent = "";
    output.textContent = "";
  }

  static validateNumber(number) {
    const re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/;
    return re.test(number);
  }

  static validate() {
    const $phoneError = $("#phone-error");
    const number = $("#phone").val();
    $phoneError.text("");

    if (!RegistrationForm.validateNumber(number)) {
      $phoneError.text("Invalid phone number.");
    } else {
      $phoneError.text("");
    }
    return false;
  }
}
