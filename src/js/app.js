import "../css/main.scss";
import { RegistrationForm } from "./registrationForm";

const form = document.getElementById("RegistrationForm");
const mobileBtn = document.querySelector("#mobileBtn");
const emailBtn = document.querySelector("#emailBtn");
const output = document.querySelector("#output");
const phoneError = document.querySelector("#phone-error");

mobileBtn.addEventListener("click", RegistrationForm.selectMobile);
emailBtn.addEventListener("click", RegistrationForm.selectEmail);

form.addEventListener("submit", function (e) {
  e.preventDefault();
  RegistrationForm.validate();
  if (
    $("#RegistrationForm").validator("validate").has(".has-error").length ===
      0 &&
    phoneError.textContent === ""
  ) {
    output.textContent = "Ready to proceed to login";
  } else {
    output.textContent = "";
  }
});
